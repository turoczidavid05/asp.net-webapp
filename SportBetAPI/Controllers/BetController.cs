﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using SportBetAPI.Data;
using SportBetAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace SportBetAPI.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    [ApiController]
    public class BetController : ControllerBase
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly IConfiguration _configuration;
        private readonly ApplicationDbContext _context;

        public BetController(UserManager<IdentityUser> userManager, IConfiguration configuration, ApplicationDbContext context)
        {
            _userManager = userManager;
            _configuration = configuration;
            _context = context;
        }
        
        [HttpGet]
        public ActionResult<IEnumerable<Bet>> Get()
        {
            return _context.Bets.OrderBy(x => x.EndTime).ToList();
        }
        
        [HttpGet("{id}")]
        public ActionResult<Bet> Get(string id)
        {
            return _context.Bets.FirstOrDefault(x => x.Uid == id);
        }
        
        [HttpPost]
        public async Task<IActionResult> PostAsync([FromBody] Bet value)
        {
            value.Uid = Guid.NewGuid().ToString();
            value.CreateTime = DateTime.Now;

            var claimsIdentity = this.User.Identity as ClaimsIdentity;
            var userId = claimsIdentity.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            var myself = _context.Users.FirstOrDefault(x => x.Email == userId);

            var match = _context.Matches.Where(x => x.Uid == value.MatchUID).FirstOrDefault();
            value.Match = match;
            value.EndTime = match.EndDate;
            
            if (value.W1Team)
            {
                value.Occ = match.T1WOcc;
            }
            if (value.W2Team)
            {
                value.Occ = match.T2WOcc;
            }
            if (value.Draw)
            {
                value.Occ = match.DrawOcc;
            }

            value.ExpectedPrize = value.BetMoney * value.Occ;

            value.User = myself as SiteUser;
            _context.Bets.Add(value);
            _context.SaveChanges();
            
            return Ok();
        }
    }
}
