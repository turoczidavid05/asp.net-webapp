﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using SportBetAPI.Data;
using SportBetAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SportBetAPI.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    [ApiController]
    public class SportController : ControllerBase
    {
        private readonly UserManager<IdentityUser> userManager;
        private readonly IConfiguration configuration;
        private readonly ApplicationDbContext context;

        public SportController(UserManager<IdentityUser> userManager, IConfiguration configuration, ApplicationDbContext context)
        {
            this.userManager = userManager;
            this.configuration = configuration;
            this.context = context;
        }

        [HttpGet]
        public ActionResult<IEnumerable<Sport>> Get()
        {
            return this.context.Sports;
        }

        [HttpPost]
        public IActionResult Post([FromBody] Sport value)
        {
            value.Uid = Guid.NewGuid().ToString();

            this.context.Sports.Add(value);
            this.context.SaveChanges();

            return Ok();
        }
    }
}
