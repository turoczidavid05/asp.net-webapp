﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Configuration;
using SportBetAPI.Data;
using SportBetAPI.Hubs;
using SportBetAPI.Models;

namespace SportBetAPI.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    [ApiController]
    public class MatchController : ControllerBase
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly IConfiguration _configuration;
        private readonly ApplicationDbContext context;
        private readonly IHubContext<MatchHub> _hub;

        public MatchController(UserManager<IdentityUser> userManager, IConfiguration configuration, ApplicationDbContext context, IHubContext<MatchHub> hub)
        {
            _userManager = userManager;
            _configuration = configuration;
            this.context = context;
            _hub = hub;
        }
        
        [HttpGet]
        public ActionResult<IEnumerable<Match>> Get()
        {
            return context.Matches;
        }

        [HttpPost]
        public async Task<IActionResult> PostAsync([FromBody] Match value)
        {
            value.Uid = Guid.NewGuid().ToString();

            var claimsIdentity = this.User.Identity as ClaimsIdentity;
            var userId = claimsIdentity.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            var myself = await _userManager.GetUserAsync(this.User);
            
            //value.Seller = myself;
            context.Matches.Add(value);
            context.SaveChanges();

            await _hub.Clients.All.SendAsync("NewMatch", value);
            return Ok();
        }

        [HttpPut("{id}")]
        public void Put([FromBody] Match value, string id)
        {
            var old = context.Matches.FirstOrDefault(x => x.Uid == id);
            value.Uid = id;
            context.Remove(old);
            context.Add(value);
            context.SaveChanges();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(string id)
        {
            var old = await context.Matches.FindAsync(id);
            context.Remove(old);
            context.SaveChanges();
            return Ok();
        }
    }
}
