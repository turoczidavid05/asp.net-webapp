﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SportBetAPI.Migrations
{
    public partial class second : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Bets_AspNetUsers_SiteUserId",
                table: "Bets");

            migrationBuilder.RenameColumn(
                name: "SiteUserId",
                table: "Bets",
                newName: "UserUID");

            migrationBuilder.RenameIndex(
                name: "IX_Bets_SiteUserId",
                table: "Bets",
                newName: "IX_Bets_UserUID");

            migrationBuilder.AddForeignKey(
                name: "FK_Bets_AspNetUsers_UserUID",
                table: "Bets",
                column: "UserUID",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Bets_AspNetUsers_UserUID",
                table: "Bets");

            migrationBuilder.RenameColumn(
                name: "UserUID",
                table: "Bets",
                newName: "SiteUserId");

            migrationBuilder.RenameIndex(
                name: "IX_Bets_UserUID",
                table: "Bets",
                newName: "IX_Bets_SiteUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Bets_AspNetUsers_SiteUserId",
                table: "Bets",
                column: "SiteUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
