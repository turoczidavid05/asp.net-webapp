﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SportBetAPI.Models
{
    public class Bet
    {
        [Key]
        public string Uid { get; set; }
        public double Occ { get; set; }
        public double BetMoney { get; set; }
        public double ExpectedPrize { get; set; }
        public bool W1Team { get; set; }
        public bool W2Team { get; set; }
        public bool Draw { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime EndTime { get; set; }
        public bool Active { get; set; }

        [NotMapped]
        public virtual Match Match { get; set; }
        public string MatchUID { get; set; }

        public virtual SiteUser User { get; set; }
        public string UserUID { get; set; }
    }
}
