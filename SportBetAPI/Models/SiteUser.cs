﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SportBetAPI.Models
{
    public class SiteUser : IdentityUser
    {
        public double Money { get; set; }

        public virtual ICollection<Bet> Bets { get; set; }
    }
}
