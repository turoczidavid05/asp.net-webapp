﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SportBetAPI.Models
{
    public class Match
    {
        [Key]
        public string Uid { get; set; }
        [StringLength(50)]
        public string Team1 { get; set; }
        [StringLength(50)]
        public string Team2 { get; set; }
        [StringLength(50)]
        public string Sport { get; set; }
        public double T1WOcc { get; set; }
        public double T2WOcc { get; set; }
        public double DrawOcc { get; set; }
        public bool Active { get; set; }
        public bool ResultT1Win { get; set; }
        public bool ResultT2Win { get; set; }
        public bool ResultDraw { get; set; }
        public DateTime EndDate { get; set; }

        public virtual ICollection<Bet> Bets { get; set; }
    }
}
