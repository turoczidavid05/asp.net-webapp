﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SportBetAPI.Models
{
    public class Sport
    {
        [Key]
        public string Uid { get; set; }
        [StringLength(50)]
        public string Name { get; set; }
    }
}
