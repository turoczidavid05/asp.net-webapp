import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Bet} from '../bet';
import {Router} from '@angular/router';
import {Match} from '../match';
import {Betinput} from '../betinput';

@Component({
  selector: 'app-bet',
  templateUrl: './bet.component.html',
  styleUrls: ['./bet.component.css']
})
export class BetComponent implements OnInit {

  private http: HttpClient;
  private router: Router;
  private token: string | null;
  public Bets: Array<Betinput>;
  private Matches: Array<Match>;
  constructor(http: HttpClient, router: Router) {
    this.Bets = new Array<Betinput>();
    this.Matches = new Array<Match>();
    this.router = router;
    this.http = http;
    this.token = sessionStorage.getItem('token');
    if (this.token == null || this.token.toString().length < 3) {
      this.router.navigate(['login']);
    }
    else {
      const headers = {
        'Content-Type' : 'application/json',
        Authorization: 'Bearer ' + this.token
      };

      this.http.get<Betinput[]>('https://localhost:44331/api/bet', {headers}).subscribe( resp => {
       this.Bets = resp;
       console.log(resp);

       this.http.get<Match[]>('https://localhost:44331/api/match', {headers}).subscribe(response => {
          console.log(response);
          this.Matches = response;

          let i;
          for (i = 0; i < this.Bets.length; i++) {
            let j;
            for (j = 0; j < this.Matches.length; j++){
              if (this.Bets[i].matchUID === this.Matches[j].uid) {
                this.Bets[i].match = this.Matches[j];
              }
            }
          }

        }, error => {
          if (error.status.toString() === '401') {
            this.router.navigate(['login']);
          }
        });
      }, error => {
        if (error.status.toString() === '401') {
          this.router.navigate(['login']);
        }
      });
    }
  }

  ngOnInit(): void {
  }

}
