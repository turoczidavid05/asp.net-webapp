import { Component, OnInit } from '@angular/core';
import {User} from '../user';
import {HttpClient} from '@angular/common/http';
import {LoginResponse} from '../login-response';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  private http: HttpClient;
  private router: Router;
  constructor(http: HttpClient, router: Router) {
    this.http = http;
    this.router = router;
  }

  login(name: HTMLInputElement, pass: HTMLInputElement): void {
    const actualuser = new User();
    actualuser.username = name.value;
    actualuser.password = pass.value;

    this.http.post<LoginResponse>('https://localhost:44331/login', actualuser).subscribe(response => {
      const token = response.token;
      if (token != null && token.toString().length > 3) {
        sessionStorage.setItem('token', token);
        this.router.navigate(['/matches']);
      }
    }, error => {
      if (error.status.toString() === '401') {
        window.alert('Invalid user name or password');
      }
      else {
        window.alert('server is down');
      }
    });
  }

  ngOnInit(): void {
  }

}
