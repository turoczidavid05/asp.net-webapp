import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {Router} from "@angular/router";
import {Match} from "../match";
import {newArray} from "@angular/compiler/src/util";
import {Sport} from "../sport";

@Component({
  selector: 'app-create-bet',
  templateUrl: './create-bet.component.html',
  styleUrls: ['./create-bet.component.css']
})
export class CreateBetComponent implements OnInit {

  private http: HttpClient;
  private router: Router;
  private token: string | null;
  private matches: Array<Match>;
  private sports: Array<Sport>;
  constructor(http: HttpClient, router: Router) {
    this.http = http;
    this.router = router;
    this.token = sessionStorage.getItem('token');
    this.matches = new Array<Match>();
    this.sports = new Array<Sport>();
  }

  ngOnInit(): void {
  }

  GetSports() : void {
    const headers = {
      'Content-Type' : 'application/json',
      Authorization: 'Bearer ' + this.token
    };

    this.http.get<Match[]>('https://localhost:44331/api/sport',{ headers : headers}).subscribe(response => {
      this.matches = response;
    }, error => {
      if (error.status.toString() == '401') {
        this.router.navigate(['login']);
      }
    });
  }

  GetMatchsFilterSport(sport: HTMLInputElement) : void {
    const headers = {
      'Content-Type' : 'application/json',
      Authorization: 'Bearer ' + this.token
    };

    let params = new HttpParams().set("sport", sport.value);

    this.http.get<Match[]>('https://localhost:44331/api/match',{ headers : headers, params : params}).subscribe(response => {
      this.matches = response;
    }, error => {
      if (error.status.toString() == '401') {
        this.router.navigate(['login']);
      }
    });
  }

}
