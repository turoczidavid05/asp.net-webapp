import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import {HttpClientModule} from "@angular/common/http";
import { MatchComponent } from './match/match.component';
import { CreateMatchComponent } from './create-match/create-match.component';
import { CreateBetComponent } from './create-bet/create-bet.component';
import {FormsModule} from "@angular/forms";
import { BetComponent } from './bet/bet.component';
import { RegisterComponent } from './register/register.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MatchComponent,
    CreateMatchComponent,
    CreateBetComponent,
    BetComponent,
    RegisterComponent
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        FormsModule
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
