import { Component, OnInit } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";
import {Register} from "../register";
import {RegisterResponse} from "../register-response";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  private http: HttpClient;
  private router: Router;
  constructor(http: HttpClient, router: Router) {
    this.http = http;
    this.router = router;
  }

  Register(email: HTMLInputElement, pass: HTMLInputElement, passagain: HTMLInputElement) {
    if (pass.value == passagain.value) {
      if (email.value.length > 3) {
        const newUser = new Register(email.value, pass.value);
        this.http.post<RegisterResponse>('https://localhost:44331/register', newUser).subscribe(resp => {
          const RegName = resp.UserName;
          this.router.navigate(['login']);
        }, error => {
          console.log(error)
        });
      }
      else {
        window.alert('username.lenght > 3');
      }
    }
    else {
      window.alert('First and second password not equal');
    }
  }

  ngOnInit(): void {
  }

}
