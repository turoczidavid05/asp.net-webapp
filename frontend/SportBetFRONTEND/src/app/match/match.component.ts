import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {Match} from '../match';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Signalr} from '../signalr';
import {Bet} from '../bet';

@Component({
  selector: 'app-match',
  templateUrl: './match.component.html',
  styleUrls: ['./match.component.css']
})
export class MatchComponent implements OnInit {

  private router: Router;
  private http: HttpClient;
  private token: string | null;
  public Matches: Array<Match>;
  private BetMId!: string | null;
  private EstimatedPrice!: number;
  private OldEps!: number;
  public editMatch!: Match;

  private modal!: HTMLElement | null;

  private matchsignal!: Signalr;
  constructor(router: Router, http: HttpClient) {
    this.Matches = new Array<Match>();
    this.router = router;
    this.http = http;
    this.EstimatedPrice = 0;
    this.token = sessionStorage.getItem('token');
    this.editMatch = new Match();

    if (this.token == null || this.token.toString().length < 3) {
      this.router.navigate(['login']);
    }
    else {
      const headers = {
        'Content-Type' : 'application/json',
        Authorization: 'Bearer ' + this.token
      };

      this.http.get<Match[]>('https://localhost:44331/api/match', {headers}).subscribe(response => {
        console.log(response);
        this.Matches = response;
      }, error => {
        if (error.status.toString() === '401') {
        this.router.navigate(['login']);
        }
      });

      this.matchsignal = new Signalr('https://localhost:44331/matchHub');
      this.matchsignal.register('NewMatch', x => {
      this.Matches.push(x);
      return true;
      });
      this.matchsignal.start();
    }
  }

  ngOnInit(): void {
  }

  Bidding(uid: string): void {
    this.BetMId = uid;
  }

  ThisUID(uid: string): boolean {
    if (uid === this.BetMId) {
      return true;
    }
    return false;
  }

  SendBet(value: HTMLInputElement, predict: HTMLSelectElement): void {
    const newBet = new Bet();
    newBet.MatchUID = this.BetMId;
    newBet.BetMoney = parseInt(value.value, 10);
    if (predict.value === 'team1') {
      newBet.W1Team = true;
    }
    else {
      newBet.W1Team = false;
    }
    if (predict.value === 'team2') {
      newBet.W2Team = true;
    }
    else {
       newBet.W2Team = false;
    }
    if (predict.value === 'draw') {
      newBet.Draw = true;
    }
    else {
      newBet.Draw = false;
    }

    const headers = {
      'Content-Type' : 'application/json',
      Authorization: 'Bearer ' + this.token
    };

    this.http.post('https://localhost:44331/api/bet', newBet , {headers}).subscribe(resp => { }
    , error => {console.log(error); });

    this.BetMId = null;
  }

  showupdate(em: Match): void {
    this.editMatch.team1 = em.team1;
    this.editMatch.team2 = em.team2;
    this.editMatch.sport = em.sport;
    this.editMatch.t1WOcc = em.t1WOcc;
    this.editMatch.t2WOcc = em.t2WOcc;
    this.editMatch.drawOcc = em.drawOcc;
    this.editMatch.resultT1Win = em.resultT1Win;
    this.editMatch.resultT2Win = em.resultT2Win;
    this.editMatch.resultDraw = em.resultDraw;
    this.editMatch.uid = em.uid;
    this.editMatch.endDate = em.endDate;
  }

  saveupdate(): void {
    console.log(this.editMatch);

    const headers = {
      'Content-Type' : 'application/json',
      Authorization: 'Bearer ' + this.token
    };

    const params = new HttpParams().set('id', this.editMatch.uid);

    console.log(this.token);
    console.log(this.editMatch.uid);

    const url = 'https://localhost:44331/api/match/' + this.editMatch.uid;

    this.http.put( url , this.editMatch , {headers } ).subscribe(resp => {

    }, error => {console.log(error); });
  }

  destroy(): void {
    this.editMatch = new Match();
  }
}
