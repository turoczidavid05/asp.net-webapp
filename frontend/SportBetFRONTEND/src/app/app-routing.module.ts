import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MatchComponent} from './match/match.component';
import {CreateMatchComponent} from './create-match/create-match.component';
import {LoginComponent} from './login/login.component';
import {BetComponent} from './bet/bet.component';
import {RegisterComponent} from './register/register.component';

const routes: Routes = [
  {path: 'matches', component: MatchComponent},
  {path: 'add', component: CreateMatchComponent},
  {path: 'login', component: LoginComponent},
  {path: 'bet', component: BetComponent},
  {path: 'register', component: RegisterComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
