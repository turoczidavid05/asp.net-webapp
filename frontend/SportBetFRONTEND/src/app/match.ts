
export class Match {
  public team1!: string;
  public team2!: string;
  public sport!: string;
  public drawOcc!: string;
  public t1WOcc!: string;
  public t2WOcc!: string;
  public resultDraw!: string;
  public resultT1Win!: string;
  public resultT2Win!: string;
  public uid!: string;
  public endDate!: string;
}
