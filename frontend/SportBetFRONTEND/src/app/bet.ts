import {Match} from './match';

export class Bet {
  public BetMoney!: number;
  public MatchUID!: string | null;
  public W1Team!: boolean;
  public W2Team!: boolean;
  public Draw!: boolean;
  public Match!: Match;
}
