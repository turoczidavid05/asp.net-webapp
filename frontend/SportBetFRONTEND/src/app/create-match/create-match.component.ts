import { Component, OnInit } from '@angular/core';
import {Match} from '../match';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-create-match',
  templateUrl: './create-match.component.html',
  styleUrls: ['./create-match.component.css']
})
export class CreateMatchComponent implements OnInit {

  private http: HttpClient;
  public newMatch: Match;
  constructor(http: HttpClient) {
    this.http = http;
    this.newMatch = new Match();
  }

  ngOnInit(): void {
  }


  // tslint:disable-next-line:max-line-length
  addmatch(): void {

    const token = sessionStorage.getItem('token');

    const headers = {
      'Content-Type' : 'application/json',
      Authorization: 'Bearer ' + token
    };
    console.log(this.newMatch);
    this.http.post('https://localhost:44331/api/match', this.newMatch, {headers}).subscribe(resp => {
      this.newMatch = new Match();
    }, error => { console.log(error); });
  }

}
