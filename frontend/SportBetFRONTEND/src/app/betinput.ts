import {Match} from './match';

export class Betinput {
  public betMoney!: string;
  public draw!: string;
  public expectedPrize!: string;
  public match!: Match;
  public matchUID!: string;
  public occ!: string;
  public uid!: string;
  public user!: string;
  public userUID!: string;
  public w1Team!: string;
  public w2Team!: string;
  public createTime!: string;
  public endTime!: string;
}
